# ACTIVIDAD 6 - Javascript

La actividad consiste en crear una página web que convierta temperaturas entre Celsius, Fahrenheit, Kelvin y Rankine. Debe incluir opciones de selección, un campo de entrada, un botón de conversión y mostrar el resultado. Además, se requiere proporcionar información sobre las fórmulas utilizadas y una tabla de conversiones.

## Requisitos de la Actividad

Crea una página web que transforma la temperatura entre Celsius, Fahrenheit, Kelvin o Rankine. La página debe incluir:

- Un título y una imagen.
- Un apartado para elegir el tipo de temperatura a convertir.
- Un campo para ingresar la temperatura.
- Un botón para realizar la conversión.
- Un área para mostrar el resultado.
- Una descripción de las fórmulas utilizadas para la conversión.
- Una tabla con todas las conversiones posibles.

## Contenido del Repositorio

El repositorio contiene la siguiente estructura de carpetas y archivos:

- **assets**: Carpeta que contiene los recursos necesarios para la página web.
  - **css/**: Carpeta que contiene los archivos CSS.
  - **img/**: Carpeta que contiene las imágenes utilizadas en la página.
  - **js/**: Carpeta que contiene los archivos JavaScript.
- **index.html**: Archivo HTML que contiene la estructura de la página web.
- **Actividad-6.pdf**: Documento PDF que describe los requisitos de la actividad.
- **README.md**: Archivo README del proyecto.

## Estado

[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=brightgreen)](https://gitlab.com/gusgonza/mp04-actividad-6)
