function convert() {
    let temperature = parseFloat(document.getElementById('temperature').value);
    let fromUnit = document.getElementById('from').value;
    let toUnit = document.getElementById('to').value;
    let result;

    switch (fromUnit) {
        case 'celsius':
            switch (toUnit) {
                case 'fahrenheit':
                    result = temperature * 9 / 5 + 32;
                    break;
                case 'kelvin':
                    result = temperature + 273.15;
                    break;
                case 'rankine':
                    result = (temperature + 273.15) * 9 / 5;
                    break;
                default:
                    result = temperature;
            }
            break;
        case 'fahrenheit':
            switch (toUnit) {
                case 'celsius':
                    result = (temperature - 32) * 5 / 9;
                    break;
                case 'kelvin':
                    result = (temperature - 32) * 5 / 9 + 273.15;
                    break;
                case 'rankine':
                    result = temperature + 459.67;
                    break;
                default:
                    result = temperature;
            }
            break;
        case 'kelvin':
            switch (toUnit) {
                case 'celsius':
                    result = temperature - 273.15;
                    break;
                case 'fahrenheit':
                    result = (temperature - 273.15) * 9 / 5 + 32;
                    break;
                case 'rankine':
                    result = temperature * 9 / 5;
                    break;
                default:
                    result = temperature;
            }
            break;
        case 'rankine':
            switch (toUnit) {
                case 'celsius':
                    result = (temperature - 491.67) * 5 / 9;
                    break;
                case 'fahrenheit':
                    result = temperature - 459.67;
                    break;
                case 'kelvin':
                    result = temperature * 5 / 9;
                    break;
                default:
                    result = temperature;
            }
            break;
    }

    document.getElementById('result').innerText = `Result: ${result.toFixed(2)}`;
    updateConversionFormulae(fromUnit, toUnit);
}

function updateConversionFormulae(fromUnit, toUnit) {
    let formula;
    switch (fromUnit + "-" + toUnit) {
        case "celsius-fahrenheit":
            formula = "F = C * (9/5) + 32";
            break;
        case "celsius-kelvin":
            formula = "K = C + 273.15";
            break;
        case "celsius-rankine":
            formula = "R = C * (9/5) + 32 + 273.15";
            break;
        case "fahrenheit-celsius":
            formula = "C = (F - 32) * 5/9";
            break;
        case "fahrenheit-kelvin":
            formula = "K = (F - 32) * 5/9 + 273.15";
            break;
        case "fahrenheit-rankine":
            formula = "R = F + 459.67";
            break;
        case "kelvin-celsius":
            formula = "C = K - 273.15";
            break;
        case "kelvin-fahrenheit":
            formula = "F = K * 9/5 - 459.67";
            break;
        case "kelvin-rankine":
            formula = "R = K * 9/5";
            break;
        case "rankine-celsius":
            formula = "C = (R - 32 - 273.15) * 5/9";
            break;
        case "rankine-fahrenheit":
            formula = "F = R - 459.67";
            break;
        case "rankine-kelvin":
            formula = "K = R * 5/9";
            break;
        default:
            formula = "Select units for conversion";
    }

    document.getElementById('formula-text').innerText = formula;
}

